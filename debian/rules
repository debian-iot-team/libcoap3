#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_HOST_ARCH_OS   ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_OS)
export DEB_HOST_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
export DEB_BUILD_GNU_TYPE ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
export DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

include /usr/share/dpkg/pkg-info.mk
export REVDATE := $(shell TZ=UTC date -d@"$(SOURCE_DATE_EPOCH)" +%Y-%m-%d)

DEB_BUILD_DIR = debian/build
INSTDIR := $(CURDIR)/debian/tmp

DEB_CONFIGURE_OPTS = \
	--prefix=/usr \
	--mandir=\$${prefix}/share/man \
	--infodir=\$${prefix}/share/info \
	--htmldir=\$${prefix}/share/doc/libcoap \
	--libdir=\$${prefix}/lib/$(DEB_HOST_MULTIARCH) \
	--build=$(DEB_BUILD_GNU_TYPE) \
	--host=$(DEB_HOST_GNU_TYPE) \
	--enable-shared \
	--enable-static \
	--disable-add-default-names \
	--disable-as-dtls-default \
	--disable-documentation \
	--disable-doxygen \
	--disable-license-install \
	--disable-manpages \
	--enable-examples \
	$(NULL)

DEB_CONFIGURE_DTLS_NO_OPTS = \
	$(DEB_CONFIGURE_OPTS) \
	--disable-dtls \
	--enable-documentation \
	--enable-doxygen \
	--enable-manpages \
	$(NULL)

DEB_CONFIGURE_DTLS_OPENSSL_OPTS = \
	$(DEB_CONFIGURE_OPTS) \
	--enable-dtls \
	--with-openssl \
	$(NULL)

DEB_CONFIGURE_DTLS_GNUTLS_OPTS = \
	$(DEB_CONFIGURE_OPTS) \
	--enable-dtls \
	--with-gnutls \
	$(NULL)

PREPROCESS_FILES := $(wildcard debian/*.in)

$(PREPROCESS_FILES:.in=): %: %.in
	sed 's,/@DEB_HOST_MULTIARCH@,$(DEB_HOST_MULTIARCH:%=/%),g' $< > $@

%:
	dh $@

override_dh_auto_clean:
	dh_clean
	if [ -f Makefile ]; then \
		make clean ;\
	fi
	for i in `find $(CURDIR) -type f \
		\( -name "libcoap-3*.pc" \
		-o -name "$(PACKAGE_BASE).dirs" -o -name "$(PACKAGE_BASE)-dev.dirs" \
		-o -name "$(PACKAGE_BASE).install" -o -name "$(PACKAGE_BASE)-dev.install" \
		-o -name ".dirstamp" \
		-o -name "tags" \
		-o -name "Doxyfile" \)` ;\
	do rm -f $$i; done
	rm -f config.log config.status coap_config.h libtool stamp-h1 Makefile
	for i in `find $(CURDIR) -type d \
		\( -name ".deps" \)` ;\
	do rm -rf $$i; done

override_dh_auto_configure:
	sed -i 's/@REVDATE@/$(REVDATE)/' man/coap-*.txt.in
	mkdir -p $(DEB_BUILD_DIR)
	#
	########################################
	# configure build with no DTLS support #
	########################################
	dh_auto_configure --sourcedirectory=$(CURDIR) --builddirectory=$(DEB_BUILD_DIR)/nodtls -- $(DEB_CONFIGURE_DTLS_NO_OPTS)
	#
	#############################################
	# configure build with OpenSSL DTLS support #
	#############################################
	dh_auto_configure --sourcedirectory=$(CURDIR) --builddirectory=$(DEB_BUILD_DIR)/openssl -- $(DEB_CONFIGURE_DTLS_OPENSSL_OPTS)
	#
	############################################
	# configure build with GnuTLS DTLS support #
	############################################
	dh_auto_configure --sourcedirectory=$(CURDIR) --builddirectory=$(DEB_BUILD_DIR)/gnutls -- $(DEB_CONFIGURE_DTLS_GNUTLS_OPTS)

override_dh_auto_build:
	#
	#######################################
	# build libcoap3 with no DTLS support #
	#######################################
	dh_auto_build --sourcedirectory=$(CURDIR) --builddirectory=$(DEB_BUILD_DIR)/nodtls
	#
	############################################
	# build libcoap3 with OpenSSL DTLS support #
	############################################
	dh_auto_build --sourcedirectory=$(CURDIR) --builddirectory=$(DEB_BUILD_DIR)/openssl
	#
	###########################################
	# build libcoap3 with GnuTLS DTLS support #
	###########################################
	dh_auto_build --sourcedirectory=$(CURDIR) --builddirectory=$(DEB_BUILD_DIR)/gnutls

override_dh_auto_install: $(PREPROCESS_FILES:.in=)
	#
	####################################################
	# installing libcoap3 library with no DTLS support #
	####################################################
	dh_auto_install --destdir=$(INSTDIR) --sourcedirectory=$(CURDIR) --builddirectory=$(DEB_BUILD_DIR)/nodtls
	#
	#######################################################
	# installing libcoap3 library with no OpenSSL support #
	#######################################################
	dh_auto_install --destdir=$(INSTDIR) --sourcedirectory=$(CURDIR) --builddirectory=$(DEB_BUILD_DIR)/openssl
	#
	####################################################
	# installing libcoap3 library with no DTLS support #
	####################################################
	dh_auto_install --destdir=$(INSTDIR) --sourcedirectory=$(CURDIR) --builddirectory=$(DEB_BUILD_DIR)/gnutls
	# Find useless .la file and remove them.
	find $(INSTDIR) -name '*.la' | xargs rm


#override_dh_auto_test:
